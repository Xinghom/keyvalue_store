#!/usr/bin/env python3

from flask import Flask, request, jsonify, make_response
import os
import sys
import requests
app = Flask(__name__)

table = {}

# @app.route('/keyValue-store', methods=['GET'])
# def index():
# 	return 'Welcome to keyValue-store!'


MAINIP = "127.0.0.1"

# 1: True
# 0: Flase 
isMain = 0

if "MAINIP" in os.environ:
	isMain = 1
	MAINIP = os.environ["MAINIP"]
else:
	isMain = 0


# url test
@app.route('/test/<keyStr>', methods=['GET', 'PUT', 'DELETE'])
def test(keyStr):
	method = request.method
	print("method recieved:", method)
	#get usefull information from url
	urlTails = request.url.split(":")[2].split("/")[1:]
	print("URL revieced:", urlTails)
	mainip = "http://" + MAINIP + "/" + "/".join(urlTails)
	print(mainip)

@app.route('/keyValue-store/search/<keyStr>', methods=['GET'])
def search(keyStr):
	if keyStr not in table:
		retMSG = jsonify(msg='Error', isExist='false')
		return make_response(retMSG,
							404,
							{'Content-Type':'application/json'})
	else:
		retMSG = jsonify(msg='Success', isExist='true')
		return make_response(retMSG,
							200,
							{'Content-Type':'application/json'})
	

@app.route('/keyValue-store/<keyStr>', methods=['GET', 'PUT', 'DELETE'])
def requestRecievied(keyStr):
	method = request.method
	if isMain: # main instance
		if method == 'GET':
			return get(keyStr)
		elif method == 'PUT':
			value = request.form.get('val')
			return put(keyStr, value)
		elif method == 'DELETE':
			return remove(keyStr)
		else:
			return make_response("Method Not Allow", 405)
	else: # forwarding instance
		# - get request
		# - sent request to main
		# - get responce from main
		# - return content
		# urls = request.url
		urlTails = request.url.split(':')[2].split("/")[1:]
		mainip = "http://" + MAINIP + "/" + "/".join(urlTails)
		if method == 'GET':
			try:
				res = requests.get(mainip, timeout=3)
				return res.json()
			except requests.exceptions.RequestException as e:
				print(e)
				return mainDown()
		elif method == 'PUT':
			try:
				res = requests.put(mainip, data= {'val': request.form.get('val')}, timeout=3)
				return res.json()
			except requests.exceptions.RequestException as e:
				print(e)
				return mainDown()
		elif method == 'DELETE':
			try:
				res = requests.delete(mainip, timeout=3)
				return res.json()
			except requests.exceptions.RequestException as e:
				print(e)
				return mainDown()
		else:
			return make_response(405)


def mainDown():
	"""
	Case: Main service is down

		status code: 501
		response type: application/json
		response body:
	{
	‘result’:‘Error’,
	‘msg’:‘Server unavailable’
	}
	"""
	retMSG = jsonify(result='Error', msg='Server unavailable')
	return make_response(retMSG, 
						501,
						{"Content-Type": "application/json"})

def get(key):
	if key in table:
		val = table[key]
		retMSG = jsonify(msg='Success', value=val)
		return make_response(retMSG,
							200,
							{'Content-Type':'application/json'})			
	else:
		retMSG = jsonify(msg='Error', error='Key does not exist')
		return make_response(retMSG,
							404,
							{'Content-Type':'application/json'})
def put(key, val):
	needReplace = True if key in table else False
	table[key] = val
	if needReplace:
		retMSG = jsonify(msg='Updated successfully', replaced=1)
		return make_response(retMSG,
							200,
							{'Content-Type':'application/json'})
	else:
		retMSG = jsonify(msg='Added successfully', replaced=0)
		return make_response(retMSG,
							201,
							{'Content-Type':'application/json'})
def remove(key):
	if key in table:
		del table[key]
		retMSG = jsonify(msg='Success')
		return make_response(retMSG,
							200,
							{'Content-Type':'application/json'})
	else:
		retMSG = jsonify(msg='Error', error='Key does not exist')
		return make_response(retMSG,
							404,
							{'Content-Type':'application/json'})

if __name__ == '__main__':
    app.run(	
	    host='127.0.0.1',
		port=8080,
		debug=True
	)