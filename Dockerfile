FROM ubuntu:latest
WORKDIR /app
COPY host/keyValue-store.py /app

RUN apt-get update
RUN apt-get -y install python3 
RUN apt-get -y install curl
RUN apt-get -y install python3-flask
RUN apt-get -y install python3-pip
RUN pip3 install requests

CMD ["python3", "keyValue-store.py"]